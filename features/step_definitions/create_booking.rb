require 'rubygems'
require 'json'
require 'test/unit/assertions'
require 'faraday'
include Test::Unit::Assertions
Given(/^User want to make booking$/) do
  pending
end

When(/^User send request to API Booking$/) do
  response = Faraday.post(
      "https://restful-booker.herokuapp.com/booking",(
  {
      firstname: 'Jim',
      lastname: 'Brown',
      totalprice: 111,
      depositpaid: true,
      bookingdates: {
          checkin: '2018-01-01',
          checkout: '2019-01-01',
      },
      additionalneeds: 'Breakfast',
  }.to_json
  ),
      {
          "Content-Type" => "application/json",
          "Accept" => "application/json"
      }
  )
   assert_equal '{"bookingid":1,"booking":{"firstname":"Jim","lastname":"Brown","totalprice":111,"depositpaid":true,"bookingdates":{"checkin":"2018-01-01","checkout":"2019-01-01"},"additionalneeds":"Breakfast"}}',response.body

  p response.status
  p response.body
end

Then(/^User should see response body and assertion$/) do
  pending
end